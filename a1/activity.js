// ===== (insertOne method) =====
db.rooms.insertOne({
	name: "Single",
	accomodates: 2,
	price: 1000,
	desription: "A simple room with all the basic necessities.",
	rooms_available: 10,
	isAvailable: false
})



// ===== (insertMany method) =====
db.rooms.insertMany([
    {
		        name: "Double",
				accomodates: 3,
				price: 2000,
				desription: "A room fit for a small family going on a vacation.",
				rooms_available: 5,
				isAvailable: false
		    },
		    {
		        name: "Queen",
				accomodates: 4,
				price: 4000,
				desription: "A room with a queen sized bed perfect for a simple getaway.",
				rooms_available: 15,
				isAvailable: false
		    }
 ])



// ===== find method =====
db.rooms.find({
	name: "Double"
})



// ===== updateOne method =====
db.rooms.updateOne({
	name: "Queen"
},
	{$set: {
		name: "Queen",
		accomodates: 4,
		price: 4000,
		desription: "A room with a queen sized bed perfect for a simple getaway.",
		rooms_available: 0,
		isAvailable: false
	}
})



// ===== deleteMany method =====
db.rooms.deleteMany({
	rooms_available: 0
})